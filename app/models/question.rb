class Question < ApplicationRecord
  belongs_to :user
  has_many :answers, dependent: :destroy
  has_many :votes, dependent: :destroy
  has_many :question_tags, dependent: :destroy
  has_many :tags , through: :question_tags , dependent: :destroy
  acts_as_votable

  # def self.search(search)
  #   if search
  #     where('title LIKE ?', "%#{search}%")
  #   else
  #     []
  #   end
  # end

  def self.search(search, page)
    if search
      where('title LIKE ?', "%#{search}%").paginate(page:page, per_page:2)
    else
      paginate(page:page, per_page:2)
    end
  end
  
end
