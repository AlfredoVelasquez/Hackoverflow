class Answer < ApplicationRecord
  belongs_to :user
  belongs_to :question
  has_many :votes, dependent: :destroy
  enum status: { banned: 0, proposed: 1, accepted:2 }
  acts_as_votable
end
