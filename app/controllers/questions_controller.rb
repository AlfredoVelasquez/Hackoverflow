class QuestionsController < ApplicationController
  before_action :set_question, only: [:authenticate_user!,:show, :edit, :update, :destroy,:upvote,:downvote]
  before_action :authenticate_user!, only: [:new,:create]
 

  # GET /questions
  # GET /questions.json
  def index
    @questions_all = Question.all
    @questions = @questions_all.sort_by{|question| question.weighted_score}.reverse
    @lastQuestions = Question.all.reverse
    @tags = Tag.all
    @findQuestions = Question.search(params[:search],params[:page])
    @findQuestion = @findQuestions.sort_by{|question| question.weighted_score}.reverse
    @tagQuestions = Tag.find_by(name:"#{params[:search]}".capitalize)
    respond_to do |format|
      format.html { render :index }
      format.json { render jsonapi: @questions}
    end
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
    @relatedQuestions = @question.tags.inject([]){|pote,tag| pote.push(*tag.questions)}.uniq.shuffle
    @relatedQuestions  = @relatedQuestions.select{|qr| qr.id != @question.id}
    @answers = @question.answers.where(status:"accepted") + @question.answers.reject {|a| a.status == "accepted"}.sort_by{|answer| answer.weighted_score}.reverse
    @answer = Answer.new
    # @tags = question.tags
    @vote = Vote.find_by({votable_type: "Question",votable_id: @question.id,voter_id:current_user.id}) if(current_user)
    if(@vote)
      if(@vote.vote_flag)
        @td_class = 'far'
        @tu_class = 'fas'
      else 
        @td_class = 'fas'
        @tu_class = 'far'
      end
    else
    
      @td_class = 'far'
      @tu_class = 'far'
    end
    @question_votes = Vote.where({votable_type: :Question, votable_id:@question.id})
    @question_votes = @question_votes.select{|vote| vote.vote_flag}.size - @question_votes.select{|vote| !vote.vote_flag}.size
    respond_to do |format|
      format.html { render :show }
      format.json { render jsonapi: @question}
  
  end

end
  # GET /questions/new
  def new
    # @tags = Tag.all
    @questiontag = QuestionTag.new
    @question = Question.new
    @tags = []
    # render :layout => 'special_layout'
  end

  # GET /questions/1/edit
  def edit
    
    @questiontag = QuestionTag.where(question_id:@question.id)
    @tags = @questiontag.map{|qt| qt.tag_id}
    
  end

  # POST /questions
  # POST /questions.json
  def create
    @question = Question.new(question_params.merge!({user_id:current_user.id}))
    
    respond_to do |format|
      if @question.save
        format.html { redirect_to @question, notice: 'Question was successfully created.' }
        format.json { render :show, status: :created, location: @question }
         params[:question][:question_tag][:tag_id].each do |id| 
          if id.present?
            @questiontag = QuestionTag.create(question_id:@question.id, tag_id:id)
          end
        end
        puts params
      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
   
  end
  

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    respond_to do |format|
      @question.question_tags.map{|qt| qt.destroy}
      if @question.update(question_params)
        format.html { redirect_to @question, notice: 'Question was successfully updated.' }
        format.json { render :show, status: :ok, location: @question }
        params[:question][:question_tag][:tag_id].each do |id| 
          if id.present?
            @questiontag = QuestionTag.create(question_id:@question.id, tag_id:id)
          end
        end
        puts params
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question.destroy
    respond_to do |format|
      format.html { redirect_to questions_url, notice: 'Question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  def upvote
    @vote=Vote.find_by({votable_type:"Question",votable_id:@question.id,voter_id:current_user.id}) if current_user
    if !@vote  && current_user
      current_user.likes @question
    elsif(@vote)
      if(@vote.vote_flag)
        @vote.destroy
      else
        @vote.update(vote_flag: true)
      end
    end
    
    redirect_to question_path(@question)
  end

  def downvote
      @vote=Vote.find_by({votable_type:"Question",votable_id:@question.id,voter_id:current_user.id}) if current_user
      if !@vote  && current_user
        current_user.dislikes @question
      elsif(@vote)
        if(!@vote.vote_flag)
          @vote.destroy
        else
          @vote.update(vote_flag:false)
        end
      end 
       
      redirect_to question_path(@question)
end 
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:title, :body, tag_id: [])
    end
 
   
end
