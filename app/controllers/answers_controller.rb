class AnswersController < ApplicationController
  before_action :set_answer, only: [:show, :edit, :update, :destroy,:upvote,:downvote]
  before_action :set_question, only: [:upvote, :downvote]
  before_action :store_user_location!, if: :storable_location?
  before_action :authenticate_user!, only: [:create]
  
  # GET /answers
  # GET /answers.json
  def index
    @answers = Answer.all
  end

  # GET /answers/1
  # GET /answers/1.json
  def show
  end

  # GET /answers/new
  def new
    @answer = Answer.new
    @question = Question.find(params[:question_id])
  end

  # GET /answers/1/edit
  def edit
  end

  # POST /answers
  # POST /answers.json
  def create
    @question = Question.find(params[:question_id])
    @answer = Answer.new(answer_params.merge!({user_id:current_user.id,question:@question}))
    
    respond_to do |format|
      if @answer.save
        format.html { redirect_to @question, notice: 'Answer was successfully created.' }
        format.json { render :show, status: :created, location: @answer }
      else
        format.html { render :new }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /answers/1
  # PATCH/PUT /answers/1.json
  def update
    @question = Question.find(params[:question_id])
    if @answer.status != "accepted"
      redirect_to @question
      @answer.update(status:"accepted")
    else
      redirect_to @question
      @answer.update(status:"proposed")
    end
  end

  # DELETE /answers/1
  # DELETE /answers/1.json
  def destroy
    @answer.destroy
    respond_to do |format|
      format.html { redirect_to @question, notice: 'Answer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def upvote
    @vote=Vote.find_by({votable_type:"Answer",votable_id:@answer.id,voter_id:current_user.id}) if current_user
    if !@vote  && current_user
      current_user.likes @answer
    elsif @vote
      if(@vote.vote_flag)
        @vote.destroy
      else
        @vote.update(vote_flag: true)
      end
    end
    
    redirect_to question_path(@question)
  end

  def downvote
      @vote=Vote.find_by({votable_type:"Answer",votable_id:@answer.id,voter_id:current_user.id}) if current_user
      if !@vote && current_user
        current_user.dislikes @answer
      elsif @vote
        if(!@vote.vote_flag)
          @vote.destroy
        else
          @vote.update(vote_flag:false)
        end 
      end 
       
      redirect_to question_path(@question)
  end 
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_answer
      @answer = Answer.find(params[:id])
    end
    def set_question
      @question = Question.find(params[:question_id])
    end
    # def answer_status
    #   params.require(:answer).permit(:status)
    # end
    # Never trust parameters from the scary internet, only allow the white list through.
    def answer_params
      params.require(:answer).permit(:status, :body, :user_id, :question_id)
    end
    def storable_location?
      request.get? && is_navigational_format? && !devise_controller? && !request.xhr? 
    end

    def store_user_location!
      # :user is the scope we are authenticating
      store_location_for(:user, request.fullpath)
    end
end
